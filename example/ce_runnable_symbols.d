/+ dub.sdl:
name "dbeaengine-example2"
dependency "dbeaengine" path="../"
+/
module runnable;

/**
 * Coedit - runnable module
 * - beaengine must be setup in the library manager.
 * - click menu Run, Compile file & run.
 * - demo for the symbol table.
 */

import std.stdio;
import disassembler;

static this()
{
    // adds all the functions of this module to the symbol table
    version(none)
        symbolTable.addModule!runnable;
    // alternatively: by hand
    else
    {
        symbolTable.add("foo", &foo);
        symbolTable.add("bar", &bar);
        symbolTable.add("callThem", &callThem);
    }
}

bool foo(){return true;}
bool bar(){return true;}

void callThem()
{
    foo;
    bar;
}

void main(string[] args)
{
    eolMode = EolMode.cr;
    writeln(prettyDisasm(&callThem, 2));
}

